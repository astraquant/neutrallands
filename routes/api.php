<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {

    Route::group(['prefix' => 'tournament'], function (){
        Route::get('list', 'TournamentController@showTournamentList'); //returns all tournaments

        Route::post('create', 'TournamentController@store'); //creates new tournament

        Route::get('myparts','TournamentController@getToursIPart'); // returns all tournaments user take part in

        Route::get('{t_id}/leave', 'TournamentController@leaveTour'); // user leaves tournament by id

        Route::get('{id}', 'TournamentController@ShowTournamentApi'); //returns tournament json format by id

        Route::get('{id}/join', 'TournamentController@joinTournament'); //request to join tournament by id
        // returns new log in users_tournaments

        Route::post('{id}/change', 'TournamentController@update'); //updates tournament by id

        Route::get('{id}/delete', 'TournamentController@destroy'); //deletes tournament by id

        Route::get('{id}/participators', 'TournamentController@getPartsList'); //returns tournament id, name; all its participators and stage they are at

        Route::get('{id}/requests', 'TournamentController@getPartRequests'); //returns all unapproved part request to tournament with id

        Route::get('{t_id}/request/{r_id}/decline', 'TournamentController@declinePartRequest');//deletes part request by request_id

        Route::get('{t_id}/request/{r_id}/accept', 'TournamentController@acceptPartRequest');//updates part request by request_id: approved = 1

        Route::get('{t_id}/kick/{u_id}', 'TournamentController@acceptPartRequest');//deletes participation by user_id

    });


    Route::post('users', 'UserController@index'); //returns all users

    Route::post('user/{id}', 'UserController@show'); //returns user by id

    Route::post('user/{id}/delete', 'UserController@delete');

    Route::post('user/addfriend/{id}', 'UserController@addFriend');

    Route::post('user/addfriend/{id}/accept','UserController@acceptFriend');

    Route::post("friends/{id}", 'UserController@showFriends');

    Route::middleware('admin')->group(function (){
        Route::post("setpermissions", 'UserController@changePermissions');
    });

    Route::post("friends/{id}/info", 'UserController@showFriendsInfo');

    Route::resource('games','GameController');

    Route::resource('meetings','MeetingController');

    Route::group([
        'prefix' => 'meetings'
        ],
        function (){
            Route::get('meeting','MeetingController@ShowMy');

            Route::get('meeting/{id}','MeetingController@meetingPage');

            Route::get('meeting/public','MeetingController@showPublic'); //show all public meetings

            Route::post('meeting/{id}/accept','MeetingController@meetingAccept');

            Route::post('meeting','MeetingController@store');
    });
    Route::group([
        'prefix' => 'messages'
    ], function () {
        Route::post('message', 'MessagesController@message');
        Route::post('all', 'MessagesController@all');
        Route::post('allFromChat', 'MessagesController@allFromChat');
        Route::post('allByDate', 'MessagesController@allByDate');
        Route::post('delete', 'MessagesController@delete');
    });
});





Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

