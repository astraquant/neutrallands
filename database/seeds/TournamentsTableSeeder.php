<?php

use Illuminate\Database\Seeder;
use App\Tournament;

class TournamentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class,1)->create();
        factory(\App\TourType::class,2)->create();
        factory(\App\Tournament::class,20)->create()->each(function ($tournament){
            $tournament->parts()->attach(factory(\App\User::class,1)->create());
        });
    }
}
