<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GamesTableSeeder::class);
        factory(App\User::class, 50)->create()->each(function ($user) {
            $user->outcoming_message()->associate(factory(App\Message::class,20)->create(['to' => $user->id-1,'from' => $user->id]));
        });

        $this->call(TournamentsTableSeeder::class);
    }
}
