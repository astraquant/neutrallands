<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTournamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournaments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->default('Unnamed');
            $table->string('description');
            $table->string('conditions');
            $table->unsignedBigInteger('type');
            $table->dateTime('start_date');
            $table->integer('current_stage')->default(0);
            $table->dateTime('next_stage_date');
            $table->integer('max_players');
            $table->unsignedBigInteger('creator');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournaments');
    }
}
