<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Game;
use Faker\Generator as Faker;

$factory->define(Game::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
        'rules' => $faker->realText($maxNbChars = 500, $indexSize = 1),
        'playercount' => $faker->numberBetween($min = 1, $max = 10),
        'rating' => $faker->numberBetween($min = 0, $max = 5),
        'popularity'=> $faker->numberBetween($min = 0, $max = 100000),
    ];
});
