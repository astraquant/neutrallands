<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Tournament;
use Faker\Generator as Faker;

$factory->define(Tournament::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
        'description' => $faker->paragraph(1),
        'conditions' => $faker->paragraph(1),
        'start_date' => $faker->dateTime(),
        'current_stage' => $faker->numberBetween(0,5),
        'next_stage_date' => $faker->dateTime(),
        'max_players' => '8',
        'creator' => '1',
        'type' => '1'
    ];
});
