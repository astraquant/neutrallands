<?php
namespace App\Repositories\Interfaces;
interface MessageInterface{
    public function getMessageById($id);
    public function send(array $data);
    public function delete($id);
    public function getByID($id);
    public function update($id,array $data);
    public function all($from,$to);
    public function all_from_chat($chatid);
    public function allByDate($fromdate,$todate,$from,$to);
    public function allByDateFromChat($from,$to,$chatid);
}
