<?php

namespace App\Repositories;
use App\Message as Model;
use App\Repositories\Interfaces\MessageInterface;
use App\Repositories\BaseRepository;
use Carbon\Carbon;

class MessageRepository implements MessageInterface
{
    protected $model;
    public function __construct()
    {
        $this->model = app(Model::class);
    }
    public function model()
    {
        return Message::class;
    }
    public function all($from,$to){
        $data = $message = $this->model->where('chatid',null)->where('from',$from)->where('to',$to)->select('id','readed','created_at','text')->get()->toArray();
        return $data;
    }
    public function send(array $data)
    {

    }

    public function delete($id)
    {
        $data  = $this->model->where('id',$id)->delete();
        return $data;
    }

    public function getByID($id)
    {

    }

    public function update($id, array $data)
    {

    }

    public function getMessageById($id)
    {
        $message = $this->model->find($id);
        return $message;
    }

    public function all_from_chat($chatid)
    {
        $data = $message = $this->model->where('chatid',$chatid)->select('id','readed','created_at','text')->get()->toArray();
        return $data;
    }

    public function allByDate($fromdate, $todate, $from,$to)
    {
        $fromd =Carbon::parse($fromdate);
        $tod =Carbon::parse($todate);
        $data = $message = $this->model->where('from',$from)->where('to',$to)->whereBetween('created_at', [$fromd, $tod])->select('id','readed','created_at','text')->get()->toArray();
        return $data;
    }

    public function allByDateFromChat($from, $to, $chatid)
    {
        $fromd =Carbon::parse($from);
        $tod =Carbon::parse($to);
        $data = $message = $this->model->where('chatid',$chatid)->whereBetween('created_at', [$fromd, $tod])->select('id','readed','created_at','text')->get()->toArray();
        return $data;
    }

}
