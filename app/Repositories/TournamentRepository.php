<?php


namespace App\Repositories;
use App\Tournament;


class TournamentRepository extends BaseRepository
{
    protected $model;

    /**
     * @return string
     */
    protected function getModelClass()
    {
        return 'App\Tournament';
    }

    /**
     * @param $id
     * @param $data
     * @return Model
     */
    public function update($id,$data){
        $this->startCondition()
            ->where('id',$id)
            ->update($data);
        $tournament = $this->startCondition()
            ->where('id',$id)
            ->first();
        return $tournament;
    }
}
