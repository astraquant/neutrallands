<?php

namespace App\Repositories;
use App\Game;

class GameRepository extends BaseRepository{
    /**
     * @var Model
     */
    protected $model;

    /**
     * @return string
     */
    protected function getModelClass()
    {
        return 'App\Game';
    }

    /**
     * @param $id
     * @param $data
     * @return Model
     */
    public function update($id,$data){
        $this->startCondition()
            ->where('id',$id)
            ->update($data);
        $game = $this->startCondition()
            ->where('id',$id)
            ->first();
        return $game;
    }
}
