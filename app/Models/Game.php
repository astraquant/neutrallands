<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $fillable = [
        'name', 'rules', 'playercount', 'rating', 'popularity',
    ];

    protected $hidden = [
      'updated_at','created_at',
    ];
}
