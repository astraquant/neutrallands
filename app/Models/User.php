<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Message;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable,HasApiTokens;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'f_name', 'l_name', 'town', 'age', 'email', 'password', 'activation_token', 'created_at', 'updated_at', 'permissions'
    ];

    protected $visible = [
        "id", 'username', 'f_name', 'l_name', 'town', 'age', 'email', 'level', 'permissions'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function outcoming_message(){
        return $this->belongsTo(Message::class, 'from');
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function friends()
    {
        return $this->belongsToMany(User::class,'friends','first_id','second_id')->withPivot('accepted');
    }

    public function tours(){
        return $this->belongsToMany(Tournament::class, 'users_tournaments', 'user_id','trnm_id')->withPivot('approved','on_stage');
    }
}
