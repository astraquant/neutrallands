<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    protected $fillable = [
        'name', 'description', 'conditions', 'type', 'start_date', 'next_stage_date', 'max_players', 'creator', 'created_at', 'updated_at'
    ];

    protected $visible = [
        'id','name', 'description', 'conditions', 'type', 'start_date', 'next_stage_date', 'max_players', 'creator', 'created_at', 'updated_at'
    ];

    public function type(){
        return $this->hasOne('App\TourType','id','type');
    }

    public function creator(){
        return $this->hasOne('App\User','id','creator');
    }

    public function parts(){
        return $this->belongsToMany(User::class,'users_tournaments','trnm_id')->withPivot('approved');
    }
}
