<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\MessageRepository;
class Message extends Model
{

    public function from()
    {
        return $this->hasOne('App\User','from');
    }
    public function to()
    {
        return $this->hasOne('App\User','to');
    }
}
