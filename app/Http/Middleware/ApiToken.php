<?php

namespace App\Http\Middleware;

use App\Model\User;
use Closure;

class ApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($token = $request->bearerToken()){
            $user = User::where('api_token',$token)->toBase()->first('id');
            if ($user){
                return $next($request);
            }
            else {
                return response()->json([
                    'msg'=>'No API Token presented'
                ],403);
            }
        }
    }
}
