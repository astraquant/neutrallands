<?php

namespace App\Http\Controllers;

use App\Repositories\MessageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessagesController extends Controller
{
    private $MessageRepository;
    public function __construct()
    {
        $this->MessageRepository = app(MessageRepository::class);
    }
    public function test(){
        $test = $this->MessageRepository->model();
        dd($test);
    }
    public function send(){

    }
    public function all(Request $request){
        //$user = Auth::id;
        $request->validate([
            'from' => 'required|max:255',
            'to' => 'required',
        ]);
        $from = $request->from;
        $to = $request->to;
        $messages = $this->MessageRepository->all($from,$to);
        return response()->json(["messages"=>$messages], 200);
    }
    public function allFromChat(Request $request){
        $request->validate([
            'chatid' => 'required|max:255'
        ]);
        //$user = Auth::id;
        $id = $request->chatid;
        $messages = $this->MessageRepository->all_from_chat($id);
        return response()->json(["messages"=>$messages], 200);
    }
    public function message(Request $request){
        $request->validate([
            'id' => 'required|max:255'
        ]);
        if($request->isMethod("GET")){
            $id = $request->id;
            $message = $this->MessageRepository->getMessageById($id);
            return response()->json(["message"=>$message], 200);
        }
    }
    public function allByDate(Request $request){
        $request->validate([
            'fromdate' => 'required|max:255',
            'todate'=>'required'
        ]);
        $fromdate = $request->fromdate;
        $todate = $request->todate;
        if ($request->has('chatid')) {
            $messages = $this->MessageRepository->allByDateFromChat($fromdate,$todate,$request->chatid);
            return response()->json(["messages"=>$messages], 200);
        }
        $from = $request->from;
        $to = $request->to;
        $messages = $this->MessageRepository->allByDate($fromdate,$todate,$from,$to);
        return response()->json(["messages"=>$messages], 200);
    }
    public function delete(Request $request){
        $request->validate([
            'id' => 'required|max:255'
        ]);
        $st = $this->MessageRepository->delete($request->id);
        return response()->json(["messages"=>$st], 200);
    }
}
