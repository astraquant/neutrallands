<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\User;
use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    public function index(UserRepository $userRepository)
    {
        $users = $userRepository->allUsers();

        return response()->json([
           $users
        ]);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show(UserRepository $userRepository, $id)
    {
        $user = $userRepository->userById($id);
        if (empty($user)) abort(404);
        return response()->json([
            $user
        ]);
    }

    /**
     * @param UserRepository $userRepository
     * @param $friend
     * @return int|string
     */
    public function addFriend(UserRepository $userRepository, $friend)
    {
        $thisUser = Auth::id();
        if($thisUser == $friend) {
            return "i can't do this";
        }
        return $userRepository->addFriend($thisUser, $friend);
    }

    /**
     * @param UserRepository $userRepository
     * @param $id
     * @return int|string
     */
    public function acceptFriend(UserRepository $userRepository, $id)
    {
        $id0 = Auth::id();
        if($id == $id0) return "i can't do this";
        return $userRepository->acceptFriend($id0, $id);
    }

    /**
     * @param UserRepository $userRepository
     * @param $id
     * @return int
     */
    public function  delete(UserRepository $userRepository, $id)
    {
        return $userRepository->deleteUser($id);
    }

    /**
     * @param UserRepository $userRepository
     * @param $id
     * @return array
     */
    public function showFriends(UserRepository $userRepository, $id)
    {
        return $userRepository->showFriends($id);
    }

    /**
     * @param UserRepository $userRepository
     * @param $id
     * @return JsonResponse
     */
    public function showFriendsInfo(UserRepository $userRepository, $id)
    {
        $info = $userRepository->showFriendsInfo($id);
        return response()->json([
            $info,
        ]);
    }

    /**
     * @param Request $request
     * @param UserRepository $userRepository
     * @return int
     */
    public function changePermissions(Request $request, UserRepository $userRepository)
    {
        $user = $request->id;
        $newP = $request->newP;
        return $userRepository->changePermissions($user, $newP);
    }
}
