<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReqisterFormRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpseclib\Crypt\Hash;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('username','password');
        if (!Auth::attempt($credentials)){
             return response()->json([
                 'msg'=>'Login failed'
             ],422);
        }
        $user = Auth::user();
        $token = $user->createToken('Personal Access Token');
        $token->token->expires_at = $request->remember_me?
            Carbon::now()->addMonth():
            Carbon::now()->addDay();
        $token->token->save();
        return response()->json([
            'token-type'=>'Bearer',
            'token' =>$token->accessToken,
            'expires_at'=>Carbon::parse($token->token->expires_at)->toDateString(),
        ],200);
    }
}
