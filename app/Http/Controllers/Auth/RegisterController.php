<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReqisterFormRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    public function __invoke(ReqisterFormRequest $request)
    {
        $user = User::create([
            'username' => $request->login,
            'email'=> $request->email,
            'f_name'=> $request->f_name,
            'l_name'=> $request->l_name,
            'password' => Hash::make($request->password),
            'activation_token'=>Str::random(30),
        ]);

        return response()->json([
            'msg'=>'success'
        ],200);
    }
}
