<?php

namespace App\Http\Controllers;

use App\Meeting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MeetingController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Meeting $meetingM
     * @return JsonResponse
     */
    public function store(Request $request, Meeting $meetingM)
    {
        $data = $request->only('game_id','place','holding','private');
        $user_id = $request ->only('user_id');
        $meeting = $meetingM->query()->create($data);
        $newusmeet = DB::table('user_meeting')->insert([
            ['meeting_id'=>$meeting->id,'user_id'=>$user_id['user_id'],'accepted'=>false],
            ['meeting_id'=>$meeting->id,'user_id'=>Auth::id(),'accepted'=>true],
        ]);
        $data = [$meeting, $newusmeet];
        return response()->json([
            "data" => $data,
        ]);
    }

    /**
     * @param Meeting $meeting
     * @return JsonResponse
     */
    public function showPublic(Meeting $meeting){
        $publicMeetings = $meeting->query()->where('private','false')->get();
        return response()->json([
            "meeting" => $publicMeetings,
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function showMy(){
        $myMeetings = DB::table('user_meeting')->where('user_id',Auth::id());
        $myMeetingsRequests = $myMeetings->where('accepted',false);
        $myActiveMeetings = $myMeetings->where('accepted',true);
        $data = [$myActiveMeetings,$myMeetingsRequests];
        return response()->json([
            "data" => $data,
        ]);
    }

    /**
     * @param Meeting $meetingM
     * @param $id
     * @return JsonResponse
     */
    public function meetingPage(Meeting $meetingM,$id){
        $linking = DB::table('user_meeting')->where('meeting_id',$id)->get();
        foreach ($linking as $item) {
            if ($item->user_id = Auth::id()){
                $meeting = $meetingM->query()->findOrFail($id);
                return response()->json([
                    "meeting" => $meeting
                ]);
            }
        }
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function meetingAccept($id){
        $meeting = DB::table('user_meeting')->where([
                                                          ['user_id', Auth::id()],
                                                            ['meeting_id',$id]
        ])
    ->update(['accepted' => true]);

        return response()->json([
            'meeting' => $meeting,
        ]);
    }
}
