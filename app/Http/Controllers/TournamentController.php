<?php

namespace App\Http\Controllers;

use App\Tournament;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Repositories\TournamentRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TournamentController extends Controller
{
    /**
     * @param Request $request
     * @param Tournament $tournament
     * @return JsonResponse
     */
    public function store(Request $request, Tournament $tournament)
    {
        $data = $request->only('name','description','conditions','type','start_date','next_stage_date','max_players');
        $data['creator'] = Auth::id();
        $tournament = $tournament->query()->create($data);

        return response()->json([
            'tournament' => $tournament,
            ]);
    }

    /**
     * updates tournament, if it exists, after check (creator id == user id)
     *
     * @param \Illuminate\Http\Request $request
     * @param TournamentRepository $tournamentRepository
     * @param tournament $id
     * @return JsonResponse
     */
    public function update(Request $request, TournamentRepository $tournamentRepository, Tournament $tournamentM, $id)
    {
        if($tournamentM->query()->where('id',$id)->exists()){
            $tournament = $tournamentM->query()->where('id',$id)->toBase()->get('creator');
            if($tournament[0]->creator == Auth::id()){
                $data = $request->input();
                $tour =$tournamentRepository->update($id,$data);
                return response()->json([
                    $tour
                ],200);
            } else{
                return response()->json([
                    'msg' => 'You are not creator'
                ],403);
            }
        } else{
            return response()->json([
                'msg' => 'Tournament does not exist'
            ], 404);
        }

    }

    /**
     * removes tournament and its participants, if it exists, after check (creator id == user id)
     *
     * @param Tournament $tournamentM
     * @param tournament $id
     * @return JsonResponse
     */
    public function destroy(Tournament $tournamentM, $id)
    {
        if(!$tournamentM->query()->where('id',$id)->exists()){
            return response()->json([
                'msg' => 'Tournament does not exist'
            ], 404);
        }
            $tournament = $tournamentM->query()->where('id',$id)->toBase()->get('creator');
            if(!$tournament[0]->creator == Auth::id()){
                return response()->json([
                    'msg' => 'You are not creator'
                ],403);
            }
            DB::table('users_tournaments')->where('trnm_id',$id)->delete();
            $tournamentM->query()->where('id',$id)->delete();
            return response()->json([
                'msg' => 'Tournament was deleted'
            ], 200);






    }

    /***********API************/

    /**
     * function returns one tournament by id
     *
     * @param $id
     * @return JsonResponse
     */
    public function ShowTournamentApi($id)
    {
        $tournament = Tournament::where('id',$id)->toBase()->get(['id','name','description','conditions','start_date','current_stage','next_stage_date','created_at']);
        $tour_type = Tournament::find($id)->type()->toBase()->get(['id','name']);
        $creator = Tournament::find($id)->creator()->get(['id','f_name','l_name']);
        return response()->json([
            'tournament' => $tournament,
            'tournament_type' => $tour_type,
            'creator' => $creator
        ],200);
    }

    /**
     * shows all tournaments
     *
     * @return JsonResponse
     */
    public function showTournamentList(){
        $tour = Tournament::toBase()->get();
        return response()->json($tour,200);
    }

    /**
     * создает неподтвержденную заявку на участие в турнире
     *
     * @param $id
     * @return JsonResponse
     */
    public function joinTournament($id){
        if(DB::table('tournaments')->where('id',$id)->exists()){
            $tournament = Tournament::find($id);
            $user = User::find(Auth::id());
            $tournament->parts()->attach($user);
            $res = DB::table('users_tournaments')->where([
                'user_id' => Auth::id(),
                'trnm_id' => $id
            ])->get();
            return response()->json($res,200);
        }else{
            return response()->json([
                'msg' => 'Tournament does not exist'
            ], 404);
        }

    }

    /**
     * returns list of approved participators with stage they are at
     * @param $id
     * @return JsonResponse
     */
    public function getPartsList($id){
        if(DB::table('tournaments')->where('id',$id)->exists()){
            $tournament = Tournament::where('id',$id)->get(['id','name']);
            $list = DB::table('users_tournaments')
                ->join('users','users_tournaments.user_id','=','users.id')
                ->join('tournaments','users_tournaments.trnm_id','=','tournaments.id')
                ->select('users.id as user_id','users.f_name','users.l_name','users.level','users_tournaments.on_stage')
                ->where('users_tournaments.trnm_id',$id)
                ->where('users_tournaments.approved',1)
                ->get();

            return response()->json([
                'tournament' => $tournament,
                'participators' => $list
            ],200);
        }else {
            return response()->json([
                'msg' => 'Tournament does not exist'
            ], 404);
        }
    }

    /**
     * returns list of unapproved participation requests
     * @param $id
     * @return JsonResponse
     */
    public function getPartRequests($id){
        if(DB::table('tournaments')->where('id',$id)->exists()){
            $tournament = Tournament::find($id);

            if($tournament['creator'] == Auth::id()){
                $tournament = Tournament::where('id',$id)->get(['id','name','max_players']);
                $list = DB::table('users_tournaments')
                    ->join('users','users_tournaments.user_id','=','users.id')
                    ->join('tournaments','users_tournaments.trnm_id','=','tournaments.id')
                    ->select('users_tournaments.id as request_id'
                        ,'users.id as user_id','users.f_name','users.l_name','users.level')
                    ->where('users_tournaments.trnm_id',$id)
                    ->where('users_tournaments.approved',0)
                    ->get();
                return response()->json([
                    'tournament' => $tournament,
                    'requests' => $list
                ],200);
            }else{
                return response()->json([
                    'msg' => 'You are not creator'
                ],403);
            }
        }else{
            return response()->json([
                'msg' => 'Tournament does not exist'
            ], 404);
        }
    }

    /**
     * deletes declined part request
     * @param $t_id
     * @param $r_id
     * @return JsonResponse
     */
    public function declinePartRequest($t_id,$r_id){
        $tournament = Tournament::find($t_id);
        if($tournament['creator'] == Auth::id()){
            if(DB::table('users_tournaments')->where('id',$r_id)->where('trnm_id',$t_id)->where('approved',0)->exists()){
                DB::table('users_tournaments')->where('id',$r_id)->delete();
                return response()->json([
                    'msg' => 'Request deleted'
                ],200);
            }else{

                return response()->json([
                    'msg' => 'Request does not exist or request does not belong to this tournament'
                ], 404);
            }
        }else{
            return response()->json([
                'msg' => 'You are not creator of tournament'
            ],403);
        }
    }

    /**
     * changes approved to 1 in users_tournaments
     *
     * @param $t_id
     * @param $r_id
     * @return JsonResponse
     */
    public function acceptPartRequest($t_id,$r_id){
        $tournament = Tournament::find($t_id);
        if($tournament['creator'] == Auth::id()){
            if(DB::table('users_tournaments')->where('id',$r_id)->where('trnm_id',$t_id)->where('approved',0)->exists()){
                $players = DB::table('users_tournaments')->where('trnm_id',$t_id)->where('approved',1)->count();
                if($tournament['max_players'] > $players){
                    DB::table('users_tournaments')->where('id',$r_id)->update(['approved' => 1]);
                    return response()->json([
                        'msg' => 'Request accepted'
                    ],200);
                }else{
                    return response()->json([
                        'msg' => 'Maximum of players is reached'
                    ], 200);
                }
            }else{

                return response()->json([
                    'msg' => 'Request does not exist or request does not belong to this tournament'
                ], 404);
            }
        }else{
            return response()->json([
                'msg' => 'You are not creator of tournament'
            ],403);
        }
    }

    /**
     * deletes participation of user in tournament by id
     *
     * @param $t_id
     * @param $u_id
     * @return JsonResponse
     */
    public function kickPart($t_id,$u_id){
        $tournament = Tournament::find($t_id);
        if($tournament['creator'] == Auth::id()){
            if(DB::table('users_tournaments')->where('user_id',$u_id)->where('trnm_id',$t_id)->where('approved',1)->exists()){
                DB::table('users_tournaments')->where('id',$u_id)->where('trnm_id',$t_id)->delete();
                return response()->json([
                    'msg' => 'Participant is kicked'
                ],200);
            }else{

                return response()->json([
                    'msg' => 'Participant does not exist or participant does not belong to this tournament'
                ], 404);
            }
        }else{
            return response()->json([
                'msg' => 'You are not creator of tournament'
            ],403);
        }
    }

    /**
     * return list of tournaments user take part in
     * @return JsonResponse
     */
    public function getToursIPart(){
        $list = User::find(Auth::id())->tours()->get();
        return response()->json([
            $list
        ],200);
    }

    /**
     * user leaves tournament by id
     *
     * @param $tournament_id
     * @return JsonResponse
     */
    public function leaveTour($tournament_id){
        $tournament = Tournament::find($tournament_id);
        if(DB::table('users_tournaments')->where('user_id',Auth::id())->where('trnm_id',$tournament_id)->exists()){
            DB::table('users_tournaments')->where('id',Auth::id())->where('trnm_id',$tournament_id)->delete();
            return response()->json([
                'msg' => 'You have left tournament'
            ],200);
        }else{

            return response()->json([
                'msg' => 'You did not take part in this tournament'
            ], 404);
        }
    }
}
