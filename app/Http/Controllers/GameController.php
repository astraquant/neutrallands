<?php

namespace App\Http\Controllers;

use App\Game;
use App\Repositories\GameRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class GameController
 * @package App\Http\Controllers
 */
class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Game $game
     * @return JsonResponse
     */
    public function index(Game $game)
    {
        $games = $game->query()->all();
        return response()->json([
            "games" => $games
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Game $game
     * @return JsonResponse
     */
    public function store(Request $request, Game $game)
    {
        $data = $request->input();
        $game = $game->query()->create($data);

        return response()->json([
            "game" => $game
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @param Game $game
     * @return JsonResponse
     */
    public function show($id, Game $game)
    {
        $game= $game->query()->findorfail($id);

        $gamedata = [ // importing necessery data
            'name' => $game->name,
            'rules' => $game->rules,
            'playercount' => $game->playercount,
            'rating' => $game->rating,
            'popularity'=>$game->popularity,
        ];

        return response()->json([
            'game' => $gamedata
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param GameRepository $GameRepository
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, GameRepository $GameRepository , $id)
    {
        $data = $request->input();
        $game = $GameRepository->update($id,$data);

        return response()->json($game);
    }

}
